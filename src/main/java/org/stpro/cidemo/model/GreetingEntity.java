package org.stpro.cidemo.model;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GreetingEntity {

    private static Logger logger = LoggerFactory.getLogger(ClassLoader.getSystemClassLoader().getClass().getCanonicalName());

    String firstname;
    String lastname;

    public GreetingEntity(String firstname, String lastname) {
        logger.info("Entity initialized");

        this.firstname = firstname;
        logger.debug(firstname);

        this.lastname = lastname;
        logger.debug(lastname);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String toJsonString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
