package org.stpro.cidemo.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/default")
public class DefaultController {

    @RequestMapping(path = "/greeting", method = RequestMethod.GET)
    public ResponseEntity<String> greeting(
            @RequestParam(name = "name", defaultValue = "World", required = false) String name) {
        String message = "{ \"message\": \"Hello " + name + "!\" }";
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/greeting",
            method = RequestMethod.POST,
            produces = "application/json",
            consumes = "*/*")
    public ResponseEntity<String> async() {
        String message = "{ \"message\": \"Accepted\" }";
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

}
