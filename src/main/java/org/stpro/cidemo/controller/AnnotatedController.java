package org.stpro.cidemo.controller;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.stpro.cidemo.model.GreetingEntity;

@RestController
@RequestMapping("/annotated")
public class AnnotatedController {

    private static Logger logger = LoggerFactory.getLogger(ClassLoader.getSystemClassLoader().getClass().getCanonicalName());

    @RequestMapping(
            path = "/greeting",
            method = RequestMethod.GET,
            produces = "application/json",
            consumes = "*/*")
    @Operation(summary = "Sends a greeting to you")
    @ApiResponse(responseCode = "200", description = "Erfolgreich")
    @ApiResponse(responseCode = "405", description = "Methode wird nicht untersützt")
    @ApiResponse(responseCode = "406", description = "Content type wird nicht untersützt")
    public ResponseEntity<String> greeting(
            @RequestParam(name = "firstname", defaultValue = "My", required = false) String firstname,
            @RequestParam(name = "lastname", defaultValue = "World", required = false) String lastname) {
        GreetingEntity greetingEntity = new GreetingEntity(firstname, lastname);
        String message = "{ \"message\": \"Hello " + greetingEntity.getFirstname() + " " + greetingEntity.getLastname() + "!\" }";

        System.out.println(message);
        logger.info(message);

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/async",
            method = RequestMethod.POST,
            produces = "application/json",
            consumes = "*/*")
    public ResponseEntity<String> async() {
        String message = "{ \"message\": \"Accepted\" }";
        return new ResponseEntity<>(message, HttpStatus.ACCEPTED);
    }

}
