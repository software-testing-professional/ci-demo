import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.stpro.cidemo.Application;

import java.net.URI;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@TestPropertySource("logging.level.root=DEBUG")
public class IntegrationTests {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    int randomServerPort;

    @Test
    public void greetingTest() throws Exception {

        final Gson gson = new Gson();

        // given
        final String url = "http://localhost:" + randomServerPort + "/annotated/greeting";
        URI uri = new URI(url);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Accept", "application/json");

        // when
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<String> responseEntity = this.restTemplate.getForEntity(uri, String.class);

        // then
        String responseBody = responseEntity.getBody();
        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
        Assert.assertEquals("{ \"message\": \"Hello My World!\" }", responseBody);


    }

}
