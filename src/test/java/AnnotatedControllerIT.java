import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.stpro.cidemo.Application;
import org.stpro.cidemo.controller.AnnotatedController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Profile("springit")
@ActiveProfiles("springit")
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class AnnotatedControllerIT {

    private static Logger logger = LoggerFactory.getLogger(ClassLoader.getSystemClassLoader().getClass().getCanonicalName());

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AnnotatedController annotatedController;

    @Test
    public void greetingIT() throws Exception {

        mockMvc.perform(get("/annotated/greeting")
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message").value("Hello My World!"));

    }

    @Test
    public void greetingWithParamsIT() throws Exception {

        mockMvc.perform(get("/annotated/greeting?firstname=Michael&lastname=Wellendorf")
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("message").value("Hello Michael Wellendorf!"));

    }

}
